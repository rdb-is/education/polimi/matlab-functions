function [ vcross ] = cross_op( v )
% Cross operator for a vector such that cross_op(v)*cross_op(w) is equal to
% the vector multiplication v X w.
vcross = [
         0 -v(3) v(2);
         v(3) 0 -v(1);
         -v(2) v(1) 0;
         ];
    


end

