function [answ] = askForSomething(question)
% Ask for a Y/N question and return 1 for yes and 0 for no
    
answ=-1;
while ( answ == -1 )
    answ=input(question,'s');
        
    if ischar(answ)
        if strcmpi('Y',answ)
            answ = 1;
        elseif strcmpi('N',answ)
            answ = 0;
        end
    end
end % While
end