function [ der ] = derive( fun, val, toll  )
%Restituisce il valore della derivata prima calcolata in "val"
if (toll <= eps || val <= eps) 
    error('Tolleranza e/o val troppo piccola');
end
h = toll;
der = (fun(val + h) - fun(val))/(h);

end

