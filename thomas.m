function [x,L,U] = thomas(c,e,a,b)

% function [x,L,U] = thomas(c,e,a,b)
% 
% algoritmo di Thomas per la risoluzione di sistemi lineari
% con matrice tridiagonale
%
% c sovradiagonale
% e sottodiagonale
% a diagonale principale
%

%N=size(A,1);
N=length(a);
alfa=zeros(N,1);
delta=zeros(N-1,1);


alfa(1)=a(1);

for i=2:N
      delta(i-1)=e(i-1)/alfa(i-1);
      alfa(i)=a(i)-delta(i-1)*c(i-1);
end

L=diag(ones(N,1), 0) + diag(delta, -1);
U=diag(alfa,0) + diag(c,1);

y=zeros(N,1);
y(1)=b(1);

for i=2:N
      y(i)=b(i)-delta(i-1)*y(i-1);
end

x=zeros(N,1);
x(N)=y(N)/alfa(N);

for i=N-1:-1:1
      x(i)=(y(i)-c(i)*x(i+1))/alfa(i);
end