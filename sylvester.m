function [ answ ] = sylvester( A)
%Funzione che fa un check dalla matrice in ingresso controllando il 
%determinante dei minori principali della matrice e d� in uscita 
% 1 (TRUE) se essi sono tutti maggiori di zero, 0 (False) in altri casi.
[rows, cols] = size(A);

%% Check Matrici Quadrate %%
if ( not(rows == cols))
    error('Matrice non quadrata...');
end

%% Controllo del determinante %%
i = 1;
while( (not(i>rows)) && (not(det(A(1:i,1:i))<=0)) )
    A(1:i,1:i)
    i = i+1;

end
i

if(i<=rows)
    answ = 0;
else
    answ = 1;
end

end