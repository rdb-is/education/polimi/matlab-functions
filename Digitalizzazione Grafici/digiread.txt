The file digital.zip contains four files - digital.m, randtest.bmp, randtest.dat and this file. digital.m is the matlab function which reads a bmp image and allows digitalisation using the mouse. The other two files are some random sample data to check that the digitalisation works. 

The command for matlab is :
[mat1,mat2]=digital;

where mat1 is an output matrix containing the digitised pixel coordinates, and mat2 is an output matrix containing the digitised values rescaled to the axes. After pressing return you will be prompted for an input file (a '*.bmp' file, although other image types could be easily added). Choose the file and then click on the top left corner, and then bottom right corner of the graph axes to be digitised with the right hand mouse button - this reads the size of the axis in pixels. You then have to enter the min/max values for each axis. Finally, you use the mouse to select points on the curve. Click, using the left hand button on these points and use the right hand button to end. You will be left with the two output matrices, and these may be interpolated etc, etc.

Because this was originally written for stratigraphic diagrams in which the independant axis is vertical and the dependant axis is horizontal, I have not added any further interpolation functions or data manipulations, but these should be easy to add. To make sure this worked with normally plotted data, a check was made with a randomly generated graph (randtest.bmp). This was scanned in, digitised and checked against the original random data giving a correlation of about 99.6%. NB Make sure the axes of the graph are as straight as possible.

This function was written using some code from the PDD set of functions written by Misrak Fisseha (CEREGE, Aix-en-Provence) for digitising pollen diagrams (Matlab 4.2). Please let me know if there are bugs in the routine or improvements to be made (the code is not going to win any prizes for elegance!). I would also be interested to hear about changes or development in the code. Thanks.

03/05/99 Simon Brewer EPD 13200 Arles France - simon.brewer@wanadoo.fr