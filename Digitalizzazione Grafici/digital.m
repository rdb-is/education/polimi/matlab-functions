function [datmat,newmat]=digital
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function [datmat,newmat]=digital 
%
% Digitalisation m-file using ginput
% outputs two matrices -
% 1 datmat - matrix of pixel values
% 2 newmat - matrix of data values calculated from axes scale
% 02/04/99 Simon Brewer

% Fileopen window
[bmpfile, bmppath] = uigetfile('*.bmp', 'Open Scanned File:');

if (bmppath == 0)
   return
   ;
else
   
   % Read in file and plot
   [X,map]=imread([bmppath,bmpfile]);
   image(X);
   axis image;
   colormap(map);
   
   
   % Get coordinates of upper left/lower right point of axes for scaling
   %
   
   h=msgbox('Click at the upper left, then the bottom right corner of the curve,      (use the right mouse button):');
   uiwait(h)
   button=0;
   while button~=3
      [x1,y1,button] = ginput(1);
      %xgo = x1;
      %ygo = y1;
   end
   button=0;
   while button~=3
      [x2,y2,button] = ginput(1);
      %xgf = x2;
      %ygf = y2;
   end
   
   % Enter max/min x and y values for scaling
   
   prompt={'Min Xval:','Max Xval:','Bottom Yval','Top Yval'};
   title='Sequence Levels';
   answer=inputdlg(prompt,title);
   minx=str2num(char(answer(1,:)));
   maxx=str2num(char(answer(2,:)));
   boty=str2num(char(answer(3,:)));
   topy=str2num(char(answer(4,:)));
   
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % Get points!
   
   h=msgbox('Ready to read curve: Left mouse button to read points; Right mouse button to stop; ESC to quit');
   uiwait(h)
   
   % Initialise
   hold on
   datmat=[];
   button = 1;
   while button == 1
      
      [xi,yi,button] = ginput(1);
      
      if(button == 3) %If right button is pressed finish digitising
         % Plot digitised data in red
         plot(datmat(:,1),datmat(:,2),'r')
         h=msgbox('Stop digitalizing! ');
         uiwait(h)
         hold off
         close
      elseif(button == 27) %If ESC is pressed
         h=msgbox(' Stop! Digitalization Interrupted......');
         uiwait(h)
         close
         clear
         break;
      else % if left button is used
         
         % Correct over/undershooting x-values
         if(xi<x1)
            xi = x1;
         end
         
         if(xi>x2)
            xi = x2;
         end
         
         % Correct over/undershooting y-values.
         if(yi>y2)
            yi = y2;
         end
         
         if(yi<y1)
            yi = y1;
         end
         
         % Mark the place where points are picked.
         plot(xi,yi,'b+')
         
         % Add values to data matrix
         datmat=[datmat; xi yi];
         
      end
      datmat=sortrows(datmat,1); %Sort X for interpolation, etc
      % datmat=sortrows(datmat,2); % For stratigraphic data
      
      %calculate scale factor for x and y axes
      scalex=(maxx-minx)/(x2-x1);
      scaley=abs((topy-boty)/(y2-y1));

      % calculate the length of bmp y-axis to reverse scales (stratigraphic data)
      lengthy=abs(y2-y1);

      % make newmat - matrix with rescaled values 
      if topy>boty % y-axis run normally (i.e. min value at base etc)
         newmat=[(((datmat(:,1)-x1).*scalex)+minx),(((lengthy-(datmat(:,2)-y1)).*scaley)+boty)];
      else % if y-axis are reversed (e.g. stratigraphic data)
         newmat=[(((datmat(:,1)-x1).*scalex)+minx),(((datmat(:,2)-y1).*scaley)+topy)];
      end
   end
end