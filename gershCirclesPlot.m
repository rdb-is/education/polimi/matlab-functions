function [ X,Y] = gershCirclesPlot( A )
% Function that given a matrix plots the Gershgorin circles 
[n,m] = size(A);

rad = zeros(n);
ths = linspace(0,2*pi,100);
% Rows Circles
X = zeros(size(ths,2),max(n,m));
Y = zeros(size(ths,2),max(n,m));


for i=1:n
    rad = sum(A(i,[1:i-1,i+1:m]));
    
    x = rad*cos(ths) + A(i,i);
    y = rad*sin(ths);
    
    X(:,i) = x;
    Y(:,i) = y;
    R(:,i) = rad;
end

%Columns

for j=i+1:i+m
    k = 1;
    rad = sum(A([1:k-1,k+1:n],k));
    
    x = rad*cos(ths) + A(k,k);
    y = rad*sin(ths);
    
    X(:,j) = x;
    Y(:,j) = y;
    R(:,j) = rad;
    
    k = k+1;
end

figure;
plot(X(:,1:n),Y(:,1:n),'b--');
hold on;
plot(X(:,n+1:end),Y(:,n+1:end),'g--');

hold off;
    
    



end

