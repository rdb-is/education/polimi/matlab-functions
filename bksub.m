% -*- texinfo -*-
% @deftypefn {Function File} {} fwsub (@var{L}, @var{b})
% La funzione fwsub risolve il sistema triangolare inferiore @var{L}*@var{x}
% = @var{b} tramite "sostituzione in avanti" e restituisce
% al chiamante il vettore delle soluzioni @var{x}
%
% @example
% @group
% L = [1 0 0; 4 6 0; 5 8 9];
% b = [ 3 5 9];
% @end group
% @group
% fwsub (L,b)
%      @result{} [ 3, -1.16667, 0.37037 ]
% @end group
% @end example
%
% @seealso{bwsub}
% @end deftypefn

% Author: Ruben Di Battista

function [x] = fwsub (L,b)
    % Suppongo matrici quadrate
    
    rows = size(L,1);
    
    %Inizializzo x
    x = zeros(rows,1);
    
    %Inizializzo 
    %Elemento che si sottrae nell'iterazione
    sott = 0;
    
    %Primo valore del vettore soluzione
    x(rows) = b(rows) / L(rows,rows);
    
    % Iterazione %
    for i= rows-1:-1:1
    x(i) = (1/L(i,i))*(b(i) - L(i,i+1:rows)*(x(i+1:rows)) );
    %Riazzero il "sottrattore" ad ogni iterazione
    sott = 0;
    
    end
    


end
