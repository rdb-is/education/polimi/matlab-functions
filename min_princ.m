function [ B ] = min_princ( A)
%Funzione che mi genera il minore principale di dimensione n-1
[row, col] = size(A);

    if(not(row==1))
        for i=row-1:-1:1
            for j=col-1:-1:1
                 B(i,j) = A(i,j);
            end
        end
    else
        B = A;
    end

end

