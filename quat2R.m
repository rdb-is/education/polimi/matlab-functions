function [ R ] = quat2R( q )
%Function that given a quaternion returns the associated rotation matrix
%(that is also the direction cosine matrix)

qcross = cross_op(q(2:4));

R = eye(3) + 2*q(1)*qcross + 2*qcross*qcross;

end

function [ vcross ] = cross_op( v )
% Cross operator for a vector such that cross_op(v)*cross_op(w) is equal to
% the vector multiplication v X w.
vcross = [
         0 -v(3) v(2);
         v(3) 0 -v(1);
         -v(2) v(1) 0;
         ];
    


end

