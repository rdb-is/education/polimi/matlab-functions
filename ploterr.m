function [] = ploterr( alpha, varargin )
% ****** [ Funzione che plotta l'andamento degli errori ] *******
[varargrow,varargcol] = size(vararrgin);
if (size(varargin)<1)
    error('Inserisci lo zero (alpha) e almeno un vettore di iterate')
end
%Inizializzo il grafico per ogni vettore di iterate
for i=1:varargcol
    figure;
    semilogy(abs(varargin(i) - alpha));
    hold on;

end

