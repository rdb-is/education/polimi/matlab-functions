function [ q ] = R2quat( R )
%This function, given a direction cosine matrix, returns the associated quaternion

qqt = 1/4*[
    trace(R)+1 2*axial(R)'
    2*axial(R) (1-trace(R))*eye(3)+R+R'
    ];

[qmax2,imax] = max(diag(qqt));

q = 1/sqrt(qmax2)*qqt(:,imax);



end

function [ v ] = axial( T )
vcross = 0.5*(T-T');

v = [
    vcross(3,2);
    vcross(1,3);
    vcross(2,1)
    ];


end