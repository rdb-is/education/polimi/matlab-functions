function [ v ] = axial( T )
% Axial operator of a tesnor that returns a vector v such that
% cross(v)=0.5*(T-T')

vcross = 0.5*(T-T');

v = [
    vcross(3,2);
    vcross(1,3);
    vcross(2,1)
    ];


end

