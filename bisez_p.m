function [ zero,xvect,it ] = bisez_p(fun,a,b,toll)

it = 1; %Setting iterates number
xk = (a+b)/2; %Guess Value

%Stopping conditions:
% - Maximum number of iterates
nmax = log2((b-a)/toll) - 1;

% - Absolute value of the function lesser than tolerance
funabs = abs(fun(xk));

while( not(it >= nmax) && not(funabs < toll))
    
        xvect(it) = xk;  
        if(fun(xk) == 0)
            break;
        
        
        elseif ((fun(a)*fun(xk))<0)
            a = a;
            b = xk;
            
        else
            a = xk;
            b = b;
        end
        
        xk = (a + b)/2; 
        it = it+1;
        

end
%Last element of xvect is
[row,col] = size(xvect);
zero = xvect(col);
it = it-1; %Because i've started from 1
end

